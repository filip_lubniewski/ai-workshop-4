package solution;

import lombok.Data;

import java.util.List;

@Data
public class CriticalPoint {

    public Double x;
    public Double y;

    public List<Integer> features;

    public CriticalPoint(Double x, Double y, List<Integer> features) {
        this.x = x;
        this.y = y;
        this.features = features;
    }
}
