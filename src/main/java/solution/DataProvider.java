package solution;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class DataProvider {

    public Set<CriticalPoint> readPointsFromFile(File input) throws FileNotFoundException {
        Set<CriticalPoint> points = new HashSet<>();
        Scanner scanner = new Scanner(input).useLocale(Locale.US);
        int featuresCount = scanner.nextInt();
        int pointsCount = scanner.nextInt();

        for(int i = 0; i < pointsCount; i++) {
            points.add(readSinglePoint(scanner, featuresCount));
        }

        return points;
    }

    private CriticalPoint readSinglePoint(Scanner scanner, int featuresCount) {
        double x = scanner.nextDouble();
        double y = scanner.nextDouble();
        skipShapeParameters(scanner);
        List<Integer> features = new ArrayList<Integer>();
        for(int i = 0; i < featuresCount; i++) {
            features.add(scanner.nextInt());
        }
        return new CriticalPoint(x, y, features);
    }

    private void skipShapeParameters(Scanner scanner) {
        for(int i = 0; i < 3;i ++) {
            scanner.next();
        }
    }
}
