package solution;

import java.awt.image.BufferedImage;

public class ImageMerged {

    public BufferedImage content;
    public int yOffset;

    public ImageMerged(BufferedImage image, int yOffset) {
        this.content = image;
        this.yOffset = yOffset;
    }
}
