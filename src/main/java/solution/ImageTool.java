package solution;

import com.sun.tools.javac.util.Pair;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Set;

public class ImageTool {

    public ImageMerged mergeImages(File upperFile, File lowerFiler) throws IOException {

        BufferedImage upperImg = ImageIO.read(upperFile);
        BufferedImage lowerImg = ImageIO.read(lowerFiler);
        BufferedImage mergedImg = new BufferedImage(Math.max(upperImg.getWidth(), lowerImg.getWidth()),
                upperImg.getHeight() + lowerImg.getHeight(), BufferedImage.TYPE_INT_RGB);

        Graphics2D g2d = mergedImg.createGraphics();
        g2d.drawImage(upperImg, 0, 0, null);
        g2d.drawImage(lowerImg, 0, upperImg.getHeight(), null);
        g2d.dispose();

        try {
            ImageIO.write(mergedImg, "png", new File("/Users/macbookpro/Desktop/semestr_6/sztuczna/lab4/concat.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ImageMerged(mergedImg, upperImg.getHeight());
    }

    public void connectCriticalPoints(ImageMerged img, Set<Pair<CriticalPoint, CriticalPoint>> points) {
        Graphics2D g2d = img.content.createGraphics();

        points.forEach(pair -> {
            g2d.setColor(new Color((int)(Math.random() * 0x1000000)));
            g2d.drawLine(pair.fst.x.intValue(), pair.fst.y.intValue(),
                    pair.snd.x.intValue(), pair.snd.y.intValue() + img.yOffset);
        });

        g2d.dispose();

        try {
            ImageIO.write(img.content, "png", new File("/Users/macbookpro/Desktop/semestr_6/sztuczna/lab4/concatWithLines.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
