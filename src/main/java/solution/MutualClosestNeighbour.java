package solution;

import com.sun.tools.javac.util.Pair;

import java.util.*;

import static java.lang.Math.*;

public class MutualClosestNeighbour {

    private Map<CriticalPoint, CriticalPoint> firstImageClosestNeighbours;
    private Map<CriticalPoint, CriticalPoint> secondImageClosestNeighbours;
    private Set<Pair<CriticalPoint, CriticalPoint>> mutualClosestNeighbours;

    public Set<Pair<CriticalPoint, CriticalPoint>> findNeighbours(Set<CriticalPoint> firstImageCriticalPoints,
                                                                  Set<CriticalPoint> secondImageCriticalPoints) {

        firstImageClosestNeighbours = findForEachPoint(firstImageCriticalPoints, secondImageCriticalPoints);
        secondImageClosestNeighbours = findForEachPoint(secondImageCriticalPoints, firstImageCriticalPoints);

        mutualClosestNeighbours = new HashSet<>();
        firstImageClosestNeighbours.forEach((criticalPoint, neighbour) -> {
            if (secondImageClosestNeighbours.containsKey(neighbour)) {
                addToClosestNeighboursIfMutual(neighbour, criticalPoint);
            }
        });

        return mutualClosestNeighbours;
    }

    private void addToClosestNeighboursIfMutual(CriticalPoint neighbour, CriticalPoint criticalPoint) {
        if (secondImageClosestNeighbours.get(neighbour).equals(criticalPoint)) {
            mutualClosestNeighbours.add(new Pair<>(criticalPoint, neighbour));
            System.out.println("Found mutual neighbours: " + criticalPoint.x + ", " + criticalPoint.y
                    + " with " + neighbour.x + ", " + neighbour.y);
        }
    }

    private Map<CriticalPoint, CriticalPoint> findForEachPoint(Set<CriticalPoint> baseImagePoints,
                                                               Set<CriticalPoint> secondImagePoints) {
        Map<CriticalPoint, CriticalPoint> neighbours = new HashMap<>();
        baseImagePoints.forEach(firstImgCriticalPoint -> {
            CriticalPoint closestNeighbour = secondImagePoints.stream()
                    .min(Comparator.comparingDouble(p -> calculateEuclideanDistance(firstImgCriticalPoint, p)))
                    .orElse(firstImgCriticalPoint);

            neighbours.put(firstImgCriticalPoint, closestNeighbour);
        });

        return neighbours;
    }

    private Double calculateEuclideanDistance(CriticalPoint first, CriticalPoint second) {
        int sum = 0;
        for (int i = 0; i < first.features.size(); i++) {
            sum += pow(first.features.get(i) - second.features.get(i), 2);
        }
        return sqrt(sum);
    }
}
