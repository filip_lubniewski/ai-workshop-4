package solution;

import com.sun.tools.javac.util.Pair;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws IOException {
        File hasi1 = new File("/Users/macbookpro/Desktop/semestr_6/sztuczna/lab4/"
                + "SI_Lab_04/images/kaczka1.png.haraff.sift");
        File hasi2 = new File("/Users/macbookpro/Desktop/semestr_6/sztuczna/lab4/"
                + "SI_Lab_04/images/kaczka2.png.haraff.sift");
        File img1 = new File("/Users/macbookpro/Desktop/semestr_6/sztuczna/lab4/"
                + "SI_Lab_04/images/kaczka1.png");
        File img2 = new File("/Users/macbookpro/Desktop/semestr_6/sztuczna/lab4/"
                + "SI_Lab_04/images/kaczka2.png");

        DataProvider dataProvider = new DataProvider();
        Set<Pair<CriticalPoint, CriticalPoint>> mutualNeighbours = new MutualClosestNeighbour().findNeighbours(
                dataProvider.readPointsFromFile(hasi1),
                dataProvider.readPointsFromFile(hasi2));

        ImageTool imageTool = new ImageTool();
        ImageMerged merged = imageTool.mergeImages(img1, img2);
        imageTool.connectCriticalPoints(merged, mutualNeighbours);
    }
}
